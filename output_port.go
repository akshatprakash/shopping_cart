package main

type OutputPort interface {
	new(rules ...PricingRule) *ShoppingCart
	calculateTotal()
	applyPromoDiscounts()
}
