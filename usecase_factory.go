package main

var productCatalogoue map[string]Product = make(map[string]Product)
var amaysimPricingRules []PricingRule
var promoCodes map[string]PromoCode = make(map[string]PromoCode)
var cart *ShoppingCart

type ShoppingCart struct {
	rules      []PricingRule
	items      map[Product]uint
	freeItems  map[Product]uint
	promoCodes []string
	total      float64
}
