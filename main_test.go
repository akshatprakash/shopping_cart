package main

import (
	"fmt"
	"github.com/DATA-DOG/godog"
	"github.com/DATA-DOG/godog/gherkin"
	"os"
	"strconv"
	"strings"
	"testing"
	"time"
)

func TestMain(m *testing.M) {
	format := "progress"
	for _, arg := range os.Args[1:] {
		if arg == "-test.v=true" { // go test transforms -v option
			format = "pretty"
			break
		}
	}
	status := godog.RunWithOptions("godogs", func(s *godog.Suite) {
		FeatureContext(s)
	}, godog.Options{
		Format:    format,
		Paths:     []string{"."},
		Randomize: time.Now().UTC().UnixNano(),
	})

	if st := m.Run(); st > status {
		status = st
	}
	os.Exit(status)
}

func iAddProducts(items *gherkin.DataTable) error {
	var fields []string
	head := items.Rows[0].Cells
	for _, cell := range head {
		fields = append(fields, cell.Value)
	}
	for i := 1; i < len(items.Rows); i++ {
		product := Product{}
		for n, cell := range items.Rows[i].Cells {
			switch head[n].Value {
			case "Product Code":
				product.ProductCode = cell.Value
			case "Product Name":
				product.ProductName = cell.Value
			case "Price":
				if !strings.HasPrefix(cell.Value, "$") {
					return fmt.Errorf("price must start with $")
				}
				priceStr := strings.TrimLeft(cell.Value, "$")
				priceFloat, err := strconv.ParseFloat(priceStr, 64)
				if err != nil {
					return err
				}
				product.Price = priceFloat
			default:
				return fmt.Errorf("unexpected column name: %s", head[n].Value)
			}
		}
		if !product.Validate() {
			return fmt.Errorf("invalid product %v", product)
		}
		productCatalogoue[product.ProductName] = product
		productCatalogoue[product.ProductCode] = product
	}
	return nil
}

func iAddRules(rules *gherkin.DataTable) error {
	var fields []string
	head := rules.Rows[0].Cells
	for _, cell := range head {
		fields = append(fields, cell.Value)
	}
	for i := 1; i < len(rules.Rows); i++ {
		pricingRule := PricingRule{Count: 1}
		for n, cell := range rules.Rows[i].Cells {
			switch head[n].Value {
			case "Count":
				if cell.Value == "" {
					continue
				}
				increment := 0
				if strings.HasSuffix(cell.Value, "+") {
					cell.Value = strings.TrimRight(cell.Value, "+")
					increment = 1
				}
				count, err := strconv.Atoi(cell.Value)
				if err != nil {
					return err
				}
				pricingRule.Count = uint(count + increment)
			case "Product Name", "Product Code":
				pricingRule.Product = productCatalogoue[cell.Value]
			case "Discount Percentage":
				if cell.Value == "" {
					continue
				}
				if !strings.HasSuffix(cell.Value, "%") {
					return fmt.Errorf("discount must end with a %%")
				}
				percentStr := strings.TrimRight(cell.Value, "%")
				percentFloat, err := strconv.ParseFloat(percentStr, 64)
				if err != nil {
					return err
				}
				pricingRule.DiscountPercentage = percentFloat
			case "Discounted Price":
				if cell.Value == "" {
					continue
				}
				if !strings.HasPrefix(cell.Value, "$") {
					return fmt.Errorf("price must start with $")
				}
				priceStr := strings.TrimLeft(cell.Value, "$")
				priceFloat, err := strconv.ParseFloat(priceStr, 64)
				if err != nil {
					return err
				}
				pricingRule.DiscountedPrice = priceFloat
			case "Free Product":
				if cell.Value == "" {
					continue
				}
				pricingRule.FreeProduct = productCatalogoue[cell.Value]
			default:
				return fmt.Errorf("unexpected column name: %s", head[n].Value)
			}
		}
		if !pricingRule.Validate() {
			return fmt.Errorf("invalid pricing rule %v", pricingRule)
		}
		amaysimPricingRules = append(amaysimPricingRules, pricingRule)
	}
	return nil
}

func iAddPromoCodes(codes *gherkin.DataTable) error {
	var fields []string
	head := codes.Rows[0].Cells
	for _, cell := range head {
		fields = append(fields, cell.Value)
	}
	for i := 1; i < len(codes.Rows); i++ {
		promoCode := &PromoCode{}
		for n, cell := range codes.Rows[i].Cells {
			switch head[n].Value {
			case "Promo Code":
				promoCode.PromoCode = cell.Value
			case "Discount Percentage":
				if !strings.HasSuffix(cell.Value, "%") {
					return fmt.Errorf("percentage must end with %%")
				}
				discountPercentageStr := strings.TrimRight(cell.Value, "%")
				discountPercentageFloat, err := strconv.ParseFloat(discountPercentageStr, 64)
				if err != nil {
					return err
				}
				promoCode.DiscountPercentage = discountPercentageFloat
			default:
				return fmt.Errorf("unexpected column name: %s", head[n].Value)
			}
		}
		if !promoCode.Validate() {
			return fmt.Errorf("invalid promo code: %v", promoCode)
		}
		promoCodes[promoCode.PromoCode] = *promoCode
	}
	return nil
}

func iAddItems(items *gherkin.DataTable) error {
	var fields []string
	head := items.Rows[0].Cells
	for _, cell := range head {
		fields = append(fields, cell.Value)
	}
	for i := 1; i < len(items.Rows); i++ {
		var err error
		count := 1
		var promoCode string
		var product Product
		for n, cell := range items.Rows[i].Cells {
			switch head[n].Value {
			case "Count":
				if cell.Value == "" {
					continue
				}
				count, err = strconv.Atoi(cell.Value)
				if err != nil {
					return err
				}
			case "Product Name", "Product Code":
				product = productCatalogoue[cell.Value]
			case "Promo Code":
				if cell.Value == "" {
					continue
				}
				promoCode = cell.Value
			default:
				return fmt.Errorf("unexpected column name: %s", head[n].Value)
			}
		}
		for j := 0; j < count; j++ {
			cart.add(product, promoCode)
		}
	}

	return nil
}

func iAddItemsCSV(itemsCSV string) error {
	itemsCSV = strings.Replace(itemsCSV, " ", "", -1)
	itemsArr := strings.Split(itemsCSV, ",")
	for _, item := range itemsArr {
		cart.add(productCatalogoue[item])
	}
	return nil
}

func iExpectCartTotal(arg1 string) error {
	if !strings.HasPrefix(arg1, "$") {
		return fmt.Errorf("expected $ before amount %s", arg1)
	}
	arg1 = strings.TrimLeft(arg1, "$")
	total, err := strconv.ParseFloat(arg1, 64)
	if err != nil {
		return err
	}
	if fmt.Sprintf("%.2f", cart.total) != fmt.Sprintf("%.2f", total) {
		return fmt.Errorf("expected $%s got $%s", fmt.Sprintf("%.2f", total), fmt.Sprintf("%.2f", cart.total))
	}
	return nil
}

func iExpectCartItems(items *gherkin.DataTable) error {
	var fields []string
	var err error
	var count int
	head := items.Rows[0].Cells
	for _, cell := range head {
		fields = append(fields, cell.Value)
	}
	var expected *ShoppingCart = &ShoppingCart{
		amaysimPricingRules,
		make(map[Product]uint),
		make(map[Product]uint),
		nil,
		0,
	}
	for i := 1; i < len(items.Rows); i++ {
		var product Product
		for n, cell := range items.Rows[i].Cells {
			switch head[n].Value {
			case "Count":
				if cell.Value == "" {
					continue
				}
				count, err = strconv.Atoi(cell.Value)
				if err != nil {
					return err
				}
			case "Product Name":
				product = productCatalogoue[cell.Value]
			default:
				return fmt.Errorf("unexpected column name: %s", head[n].Value)
			}
		}
		if !product.Validate() {
			return fmt.Errorf("invalid product %v", product)
		}
		expected.items[product] = uint(count)

	}
	for cartItem := range expected.items {
		if _, ok := cart.items[cartItem]; !ok {
			if _, ok := cart.freeItems[cartItem]; !ok {
				return fmt.Errorf("expected %v in cart not found", cartItem)
			} else if cart.freeItems[cartItem] != expected.items[cartItem] {
				return fmt.Errorf("expected %v x %d got %v x %d", cartItem, expected.items[cartItem], cartItem, cart.freeItems[cartItem])
			}
		} else if cart.items[cartItem] != expected.items[cartItem] {
			return fmt.Errorf("expected %v x %d got %v x %d", cartItem, expected.items[cartItem], cartItem, cart.items[cartItem])
		}
	}

	return nil
}

func initCart(arg1 interface{}) {
	emptyCart()
}

func FeatureContext(s *godog.Suite) {
	s.BeforeSuite(emptyCart)
	s.BeforeScenario(initCart)
	s.Step(`^products:$`, iAddProducts)
	s.Step(`^pricing rules:$`, iAddRules)
	s.Step(`^promo codes:$`, iAddPromoCodes)
	s.Step(`^items added:$`, iAddItems)
	s.Step(`^SKUs Scanned: ([^"]*)$`, iAddItemsCSV)
	s.Step(`^expected cart total ([^"]*)$`, iExpectCartTotal)
	s.Step(`^Total expected: ([^"]*)$`, iExpectCartTotal)
	s.Step(`^expected cart items:$`, iExpectCartItems)
	s.AfterScenario(clearCart)
	s.AfterSuite(emptyCart)
}

func clearCart(arg1 interface{}, err error) {
	emptyCart()
}

func emptyCart() {
	cart = nil
	productCatalogoue = make(map[string]Product)
	amaysimPricingRules = nil
	promoCodes = make(map[string]PromoCode)
	cart = &ShoppingCart{
		amaysimPricingRules,
		make(map[Product]uint),
		make(map[Product]uint),
		nil,
		0,
	}
}
