package main

import "fmt"

func (cart *ShoppingCart) add(product Product, promoCode ...string) error {
	if product == (Product{}) || (promoCode != nil && len(promoCode) == 0) {
		fmt.Errorf("nothing to add")
	}
	if _, ok := cart.items[product]; ok {
		cart.items[product]++
	} else {
		cart.items[product] = 1
	}
	if promoCodes != nil && len(promoCodes) > 0 {
		cart.promoCodes = promoCode
	}
	cart.calculateTotal()
	cart.applyPromoDiscounts()
	return nil
}
