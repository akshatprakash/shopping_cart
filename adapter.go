package main

func (cart ShoppingCart) new(rules ...PricingRule) *ShoppingCart {
	return &ShoppingCart{
		rules,
		make(map[Product]uint),
		make(map[Product]uint),
		nil,
		0,
	}
}

func (cart *ShoppingCart) calculateTotal() {
	cart.total = 0
	var freeItems = make(map[Product]uint)
	cart.freeItems = make(map[Product]uint)
	for item, count := range cart.items {
		totalled := false
		for _, rule := range amaysimPricingRules {
			if item == rule.Product && count >= rule.Count {
				if rule.DiscountPercentage > 0 {
					totalled = true
					discountedPrice := item.Price * (1 - rule.DiscountPercentage/100) * float64(count)
					cart.total += discountedPrice

					continue
				}
				if rule.DiscountedPrice > 0 {
					totalled = true
					discountedPrice := rule.DiscountedPrice * float64(count)
					cart.total += discountedPrice

					continue
				}
				if rule.FreeProduct != (Product{}) {
					freeItems[rule.FreeProduct] = count

				}
			}
		}
		if !totalled {
			cart.total += item.Price * float64(cart.items[item])

		}
	}
	cart.freeItems = freeItems
}

func (cart *ShoppingCart) applyPromoDiscounts() {
	for _, code := range cart.promoCodes {
		if _, ok := promoCodes[code]; !ok {
			continue
		}
		cart.total = cart.total * (1 - promoCodes[code].DiscountPercentage/100)
	}
}
