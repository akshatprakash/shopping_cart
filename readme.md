This project showcases ports and adapters architecture with data driven tests using cucumber .feature files

You can look at the test results by clicking the link below
[![CircleCI](https://circleci.com/bb/akshatprakash/shopping_cart.svg?style=svg)](https://circleci.com/bb/akshatprakash/shopping_cart)

To read more about the architecture go here http://alistair.cockburn.us/Hexagonal+architecture

