package main

type PricingRule struct {
	Count uint
	Product
	DiscountPercentage float64
	DiscountedPrice    float64
	FreeProduct        Product
}

type Product struct {
	ProductCode string
	ProductName string
	Price       float64
}

type PromoCode struct {
	PromoCode string
	Product
	DiscountPercentage float64
	DiscountedPrice    float64
	FreeProduct        *Product
}

func (p Product) Validate() bool {
	if p.ProductCode == "" || p.ProductName == "" {
		return false
	}
	return true
}

func (p PricingRule) Validate() bool {
	if p.Count <= 0 {
		return false
	}
	if p.DiscountPercentage > 100 {
		return false
	}
	if p.DiscountPercentage == 0 && p.DiscountedPrice == 0 && (Product{}) == p.FreeProduct {
		return false
	}
	if p.DiscountPercentage > 0 && p.DiscountedPrice == 0 && (Product{}) == p.FreeProduct {
		return p.Product.Validate()
	}
	if p.DiscountPercentage == 0 && p.DiscountedPrice > 0 && (Product{}) == p.FreeProduct {
		return p.Product.Validate()
	}
	if p.DiscountPercentage == 0 && p.DiscountedPrice == 0 && (Product{}) != p.FreeProduct {
		return p.Product.Validate()
	}
	return false
}

func (p PromoCode) Validate() bool {
	if p.PromoCode == "" && p.DiscountPercentage != 0 {
		return false
	}
	return true
}
