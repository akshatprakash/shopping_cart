Feature: add items to cart and assert total & items with rules

  Background: initialize shopping cart with the following pricing rules
    Given products:
      | Product Code | Product Name | Price    |
      | ipd          | Super iPad   | $549.99  |
      | mbp          | MacBook Pro  | $1399.99 |
      | atv          | Apple TV     | $109.50  |
      | vga          | VGA adapter  | $30.00   |
    And pricing rules:
      | Count | Product Code | Discount Percentage | Discounted Price | Free Product |
      | 3     | atv          | 33.333333%          |                  |              |
      | 3+    | ipd          |                     | $499.99          |              |
      | 1     | mbp          |                     |                  | vga          |

  Scenario: SKUs Scanned: atv, atv, atv, vga Total expected: $249.00
    Given SKUs Scanned: atv, atv, atv, vga
    Then Total expected: $249.00

  Scenario: SKUs Scanned: atv, ipd, ipd, atv, ipd, ipd, ipd Total expected: $2718.95
    Given SKUs Scanned: atv, ipd, ipd, atv, ipd, ipd, ipd
    Then Total expected: $2718.95

  Scenario: SKUs Scanned: mbp, vga, ipd Total expected: $1949.98
    Given SKUs Scanned: mbp, vga, ipd
    Then Total expected: $1979.98