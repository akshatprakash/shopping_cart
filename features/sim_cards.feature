Feature: add items to cart and assert total & items with rules

  Background: initialize shopping cart with the following pricing rules
    Given products:
      | Product Code | Product Name   | Price  |
      | ult_small    | Unlimited 1 GB | $24.90 |
      | ult_medium   | Unlimited 2 GB | $29.90 |
      | ult_large    | Unlimited 5 GB | $44.90 |
      | 1gb          | 1 GB Data-pack | $9.90  |
    And pricing rules:
      | Count | Product Name   | Discount Percentage | Discounted Price | Free Product   |
      | 3     | Unlimited 1 GB | 33.333333%          |                  |                |
      | 3+    | Unlimited 5 GB |                     | $39.90           |                |
      | 1     | Unlimited 2 GB |                     |                  | 1 GB Data-pack |
    And promo codes:
      | Promo Code | Discount Percentage |
      | I<3AMAYSIM | 10%                 |

  Scenario: 1
    Given items added:
      | Count | Product Name   | Promo Code |
      | 3     | Unlimited 1 GB |            |
      | 1     | Unlimited 5 GB |            |
    Then expected cart total $94.70
    And expected cart items:
      | Count | Product Name   |
      | 3     | Unlimited 1 GB |
      | 1     | Unlimited 5 GB |

  Scenario: 2
    Given items added:
      | Count | Product Name   | Promo Code |
      | 2     | Unlimited 1 GB |            |
      | 4     | Unlimited 5 GB |            |
    Then expected cart total $209.40
    And expected cart items:
      | Count | Product Name   |
      | 2     | Unlimited 1 GB |
      | 4     | Unlimited 5 GB |

  Scenario: 3
    Given items added:
      | Count | Product Name   | Promo Code |
      | 1     | Unlimited 1 GB |            |
      | 2     | Unlimited 2 GB |            |
    Then expected cart total $84.70
    And expected cart items:
      | Count | Product Name   |
      | 1     | Unlimited 1 GB |
      | 2     | Unlimited 2 GB |
      | 2     | 1 GB Data-pack |

  Scenario: 4
    Given items added:
      | Count | Product Name   | Promo Code |
      | 1     | Unlimited 1 GB |            |
      | 1     | 1 GB Data-pack |            |
      |       |                | I<3AMAYSIM |
    Then expected cart total $31.32
    And expected cart items:
      | Count | Product Name   |
      | 1     | Unlimited 1 GB |
      | 1     | 1 GB Data-pack |
